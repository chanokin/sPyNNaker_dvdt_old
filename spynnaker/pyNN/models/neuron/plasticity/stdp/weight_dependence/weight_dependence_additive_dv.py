from data_specification.enums import DataType
from spinn_utilities.overrides import overrides
from .abstract_weight_dependence import AbstractWeightDependence
from .abstract_has_single_scale import AbstractHasSingleScale


class WeightDependenceAdditiveDvDt(
        AbstractWeightDependence, AbstractHasSingleScale):

    # noinspection PyPep8Naming
    def __init__(self, w_min=0.0, w_max=1.0):
        AbstractWeightDependence.__init__(self)
        AbstractHasSingleScale.__init__(self)
        self._w_min = w_min
        self._w_max = w_max

    @property
    def w_min(self):
        return self._w_min

    @property
    def w_max(self):
        return self._w_max

    def is_same_as(self, weight_dependence):
        if not isinstance(weight_dependence, WeightDependenceAdditiveDvDt):
            return False
        return (
            (self._w_min == weight_dependence.w_min) and
            (self._w_max == weight_dependence.w_max) and
            (self._scale == weight_dependence.scale))

    @property
    def vertex_executable_suffix(self):
        return ""

    def get_parameters_sdram_usage_in_bytes(
            self, n_synapse_types, n_weight_terms):
        if n_weight_terms == 1:
            # int32 * 3 params * syn types
            return (4 * 3) * n_synapse_types
        else:
            raise NotImplementedError(
                "Additive weight dependence only supports one term")

    def write_parameters(
            self, spec, machine_time_step, weight_scales, n_weight_terms):

        # Loop through each synapse type's weight scale
        for w in weight_scales:
            # print("\n\nweight scale %f, min %f, max %f"%\
            #       (w, self._w_min * w, self._w_max * w))
            # Scale the weights
            spec.write_value(
                data=int(round(self._w_min * w)), data_type=DataType.INT32)
            spec.write_value(
                data=int(round(self._w_max * w)), data_type=DataType.INT32)

            # Based on http://data.andrewdavison.info/docs/PyNN/_modules/pyNN
            #                /standardmodels/synapses.html
            # Pre-multiply A+ and A- by Wmax
            spec.write_value(data=int(round(self._scale * self._w_max * w)),
                             data_type=DataType.INT32)


    @property
    def weight_maximum(self):
        return self._w_max

    @overrides(AbstractWeightDependence.get_parameter_names)
    def get_parameter_names(self):
        return ['w_min', 'w_max', 'scale']
