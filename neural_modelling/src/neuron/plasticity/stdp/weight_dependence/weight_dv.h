#ifndef _WEIGHT_DV_H_
#define _WEIGHT_DV_H_

#include "weight.h"

static weight_state_t weight_dv_apply(weight_state_t state,
                                    int32_t dv_slow);

#endif // _WEIGHT_DV_H_
